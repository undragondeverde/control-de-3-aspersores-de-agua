// Simulación de Control de 3 Aspersores de Agua
  // Autor: dragon0693
#include <Arduino.h>
const int ledPin1 = 2;
const int ledPin2 = 3;
const int ledPin3 = 4;

char Str1[3];

void setup() {
  // leds como salida:
  Serial.begin(9600);
  Serial.println("Ingresando una cadena de tres valores");
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);
  pinMode(ledPin3, OUTPUT);
}

void loop() {
  // funcion:
  while (Serial.available())
     {
        String val = Serial.readString();  
//      int val  = Serial.parseInt()- '0'; 

if (val == "000"){
  Serial.println("Los aspersores estan desactivados");
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, LOW);
  }else if (val == "101"){
  Serial.println("Se activo los aspersores 1 y 3");
  digitalWrite(ledPin1, HIGH);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, HIGH);
}else if (val == "001"){
  Serial.println("Se activo el aspersor 3");
  digitalWrite(ledPin1, LOW);
  digitalWrite(ledPin2, LOW);
  digitalWrite(ledPin3, HIGH);

}else{

  Serial.println("No estan configurados esos valores");
}}}